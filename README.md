# LGBT.io Mastodon
Hi, I'm [@self](https://lgbt.io/@self) on LGBT.io. This GitLab repository is created for a number of purposes like listing out the feature requests made by LGBT.io members.

I supposed this is going to be a lot more efficient for us in the long run as our Mastodon instance grows.

# Issues
Go to [Issues](https://gitlab.com/lgbt.io/mastodon/issues) to view all the feature requests and bug reports made by LGBT.io members.

# Blocked Instances
List will be populated soon.

# Others
...